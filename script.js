//1 
   document.addEventListener('DOMContentLoaded', function() {
   const button = document.getElementById('btn-click');
   button.addEventListener('click', function() {
     const newParagraph = document.createElement('p');
     newParagraph.textContent = 'New Paragraph';
     const contentSection = document.getElementById('content');
     contentSection.appendChild(newParagraph);
   });
 });
//2
document.getElementById('btn-input-create').addEventListener('click', function() {
   const newInput = document.createElement('input');
   newInput.type = 'text';
   newInput.placeholder = 'Введіть текст';
   newInput.name = 'userInput';
   this.parentElement.appendChild(newInput);
 });